# Aktivisda backoffice

This small streamlit webapp helps you to manage your aktivisda's instance.
You can add symbols and update config.

## Usage

The best is to use the docker image provided with all the dependencies and the `makefile`

### Getting started


```
git clone <repo_url>
cd <backend>
make init
```

Then adapt `.env` file. 
For the `AKTIVISDA_DATA` repository, you may need to create an `oauth2` token with write access to the repository.
(link: https://framagit.org/-/profile/personal_access_tokens)

### Build the docker container
```
make build
```


### Run it
```
sudo make up
```

The backoffice is available at `localhost:8501`. Aktivisda at `localhost:8080`

#### Kill it
```
sudo make kill
```

### Stop it
```
sudo make down
```

## Restart it
```
sudo make restart
```


### Execute commands

* Open the docker shell
```
make shell
```

* Execute your commands. For example.
```bash
cd aktivisda-data
git pull
git status
git add ...
git commit -m "Adding logo for xxx"
git push
```


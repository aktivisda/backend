from dotenv import load_dotenv
load_dotenv()

import streamlit as st

import src.symbols_page as symbolsPage
import src.tags_page as tagsPage
import src.templates_page as templatesPage
import src.pages.config as configPage
import src.pages.font as fontsPage
import src.router_utils as routerUtils
import src.data_utils as data_utils

st.set_page_config(page_title="Aktivisa backoffice", layout='wide')
st.title('Aktivisda backoffice')

st.sidebar.write('## Aktivisda')
pages = ['configuration', 'symbols', 'backgrounds', 'templates', 'colors', 'palettes', 'tags', 'fonts']
page = routerUtils.get_url_param_or_none('page')
page_index = pages.index(page) if page is not None and page in pages else 0
page = st.sidebar.selectbox('Please select the "page"?', pages, page_index)
routerUtils.set_url_param('page', page)

data = data_utils.Data()

if page == 'symbols':
    symbolsPage.run(data, mode=data_utils.SymbolType.SYMBOL)
elif page == 'tags':
    tagsPage.run(data)
elif page == 'backgrounds':
    symbolsPage.run(data, mode=data_utils.SymbolType.BACKGROUND)
elif page == 'templates':
    templatesPage.run(data)
elif page == 'configuration':
    configPage.run(data)
elif page == 'fonts':
    fontsPage.run(data)
else:
    st.error(f'The page { page } is not supported yet.')


st.sidebar.write('---')
st.sidebar.write('Local Dev Server: http://localhost:8080/')
st.sidebar.write('Source Code (backoffice): [Framagit](https://framagit.org/aktivisda/backend)')
st.sidebar.write('Source Code (Aktivisda): [Framagit](https://framagit.org/aktivisda/aktivisda)')
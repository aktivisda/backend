import streamlit as st
import os

from . import data_utils
from . import router_utils
from . import png_utils


SYMBOLS_DATA_FILENAME = os.getenv('SYMBOLS_DATA_FILENAME')
BACKGROUNDS_DATA_FILENAME = os.getenv('SYMBOLS_DATA_FILENAME')
I18N_LOCAL_FILES = os.getenv('I18N_LOCAL_FILES')
AKTIVISDA_DIRECTORY = os.getenv('AKTIVISDA_DIRECTORY')

def check_key(key):
    if not key:
        return False

    if ' ' in key:
        return False

    return True


def run(data: data_utils.Data):
    tags = data.tags

    st.write('''
    > Tags are used to filter `symbols` and `backgrounds`. They are very useful once you have a few dozen symbols. You have to create tags here and then add them on symbols and backgrounds page.
    ''')
    st.write('## Tag selection')
    cols1, cols2 = st.columns((2, 1))
    cols1.write('### Update existing tag')
    cols2.write('### Create new tag')
    key = cols2.text_input(f'key')

    if cols2.button('Create tag'):
        if key in tags.keys():
            cols2.error(f'Tag { key } already exist')
        elif not check_key(key):
            cols2.error(f'Please choose a valid key (no space, no special characters, lowercase) : not { key }')
        else:
            data.create_tag(key)
            st.success(f'Tag { key } sucessfully created')
            data.save()
            tags = data.tags
            key = ''
    
    selected_tag_label = router_utils.get_url_param_or_none('tag')
    sorted_tags = sorted(list(tags.keys()))
    selected_tag_index = sorted_tags.index(selected_tag_label) if selected_tag_label is not None and selected_tag_label in sorted_tags else len(sorted_tags) - 1
    tag_label = cols1.selectbox('List of tags', sorted(list(tags.keys())), selected_tag_index)
    router_utils.set_url_param('tag', tag_label)

    st.write('---')

    symbols_with_tags = data.symbols_with_tag(tag_label)
    backgrounds_with_tags = data.backgrounds_with_tag(tag_label)
    
    st.write(f'Tag **"{ tag_label }"** is used for **{ len(symbols_with_tags) }** symbols and **{ len(backgrounds_with_tags)  }** backgrounds ')
    remove_tag = st.button('Remove this tag')

    if remove_tag and len(symbols_with_tags) == 0 and len(backgrounds_with_tags) == 0:
        st.success(f'Tag **"{ tag_label }"** successfully removed.')
        data.remove_tag(tag_label)
        data.save()
    else:
        if remove_tag:
            st.error('Please remove the different occurences of this tag in symbols and backgrounds first.')

        st.write('## Translations')
        selected_tag = tags[tag_label]

        for lang in data.langs:
            selected_tag[lang] = st.text_input(f'Translation {lang}', selected_tag[lang])
        
        if st.button('Save'):
            data.update_tag(tag_label, selected_tag)
            data.save()

    st.write('---')
    if len(backgrounds_with_tags) > 0:
        with st.expander('See backgrounds with this tag'):
            filenames = [data_utils.background_preview_filename(background['preview']) for background in backgrounds_with_tags]
            urls = [f'/?page=backgrounds&background={ background["id"] }' for background in backgrounds_with_tags]
            st.markdown(png_utils.to_html_img_list(filenames, urls), unsafe_allow_html=True)

    if len(symbols_with_tags) > 0:
        with st.expander('See symbols with this tag'):
            filenames = [data_utils.symbol_preview_filename(symbol['preview']) for symbol in symbols_with_tags]
            urls = [f'/?page=symbols&symbol={ symbol["id"] }' for symbol in symbols_with_tags]
            st.markdown(png_utils.to_html_img_list(filenames, urls), unsafe_allow_html=True)

        
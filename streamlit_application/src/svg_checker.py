import streamlit as st

# Todo: replace 1e3 by 1000

def check_if_elements_are_path(svg_string):
    black_list = ['ellipse', 'line', 'polygon', 'polyline', 'text']
    for black_word in black_list:
        if '<' + black_word + ' ' in  svg_string:
            raise Exception(f'{black_word} in svg')
    return True


def check(svg_string):
    print(svg_string)
    if not check_if_elements_are_path(svg_string):
        st.error('Not all are path')

    if 'linearGradient'in svg_string:
        raise Exception('Linear Gradient in svg')
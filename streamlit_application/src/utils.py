import streamlit as st

def display_style():
    st.markdown('''
        <style>
        span.badge {
            position: absolute;
            top: -5px;
            min-width: 15px;
            min-height: 15px;
            border-radius: 50%;
            right: auto;
            border: 1px solid black;
        }
        span.color-circle {
            min-width: 15px;
            min-height: 15px;
            border-radius: 50%;
            right: auto;
            border: 1px solid black;
            display: inline-block;
        }
        </style>
        ''', unsafe_allow_html=True)
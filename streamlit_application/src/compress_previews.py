import sys
import os
import millify
import cv2
import subprocess
import uuid
import shutil
import json

def get_image_size(preview_path):
    img = cv2.imread(preview_path)
    w = img.shape[0]
    h = img.shape[1]
    return (w, h)

def resize(preview_path, w, h):
    command = f'convert { preview_path} -density 72x72 -resize {h}x{w} { preview_path }'
    subprocess.call(command, shell=True)


def compress_image(preview_path):

    if preview_path.endswith('png'):
        compress_png(preview_path)
    elif preview_path.endswith('jpg'):
        compress_jpg(preview_path)

    else:
        print('Error. Unknown format for ', preview_path) 

def compress_png(preview_path):
    assert preview_path.endswith('png')

    tmp_file = str(uuid.uuid4()) + '.png'

    command = 'pngquant "{}" --output "{}"'.format(preview_path, tmp_file)
    subprocess.call(command, shell=True, stdout=subprocess.DEVNULL)
    os.remove(preview_path)
    os.rename(tmp_file, preview_path)

def compress_jpg(preview_path):
    command = f'jpegoptim --max 75 { preview_path }'
    subprocess.call(command, shell=True, stdout=subprocess.DEVNULL)

def try_other_format(preview_path):
    preview_path_extension = preview_path.split('.')[-1]
    other_extension = 'jpg' if preview_path_extension == 'png' else 'png'  
    new_path = f'{preview_path[:-len(preview_path_extension)] }{other_extension}'
    command = f'convert { preview_path } { new_path }'
    subprocess.call(command, shell=True)
    return new_path


def compress_preview(preview_path):
    initial_weight = os.path.getsize(path)
    print(f'Compress { preview}: \n\t From: { millify.millify(initial_weight) }')

    inital_size = get_image_size(preview_path)
    if inital_size[0] >= 450 or inital_size[1] >= 450:
        if inital_size[0] >= inital_size[1]:
            resize(preview_path, 450, 10000)
        else:
            resize(preview_path, 10000, 450)

    in_other_format = try_other_format(preview_path)
    compress_image(preview_path)
    compress_image(in_other_format)

    weight_original_format = os.path.getsize(preview_path)
    weight_converted_format = os.path.getsize(in_other_format)
    convert = weight_converted_format < weight_original_format
    print(f'\t { preview_path.split(".")[-1] }: { millify.millify(weight_original_format) } { "<--" if not convert else ""}')
    print(f'\t { in_other_format.split(".")[-1] }: { millify.millify(weight_converted_format) } { "<--" if convert else ""}')

    if convert:
        os.remove(preview_path)
        return in_other_format
    else:
        os.remove(in_other_format)
        return preview_path
    
if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('Usage: python3 compress_preview.py <folder_to_previews> <path_to_templates_files>')
        sys.exit(1)

    previews_folder = sys.argv[1]
    templates_file = sys.argv[2]
    previews = os.listdir(previews_folder)
    
    f = open(templates_file)
    templates = json.load(f)
    f.close()
    print(templates)
    def find_template(templates, preview):
        for k in range(len(templates)):
            if templates[k]['preview'] == 'previews/' + preview:
                return k
        assert False

    for preview in previews:
        path = os.path.join(previews_folder, preview)

        new_preview_path = compress_preview(preview_path=path)
        if new_preview_path == preview:
            continue
        k = find_template(templates, preview)
        templates[k]['preview'] = 'previews/' + os.path.basename(new_preview_path)
    
    f = open(templates_file, 'w')
    templates = json.dump(templates, f, ensure_ascii=False, indent=2)
    f.close()
 
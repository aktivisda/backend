import sys
import os
import subprocess
import pathlib
import streamlit as st
def export_to_svg(filename: str) -> str:

    # if filename.endswith('.pnm'):
    #     return
    if filename.endswith('.svg'):
        return filename
    elif filename.endswith('.png'):
        extension = 'png'
        filename = filename[:-4]
    elif filename.endswith('.jpg'):
        extension = 'jpg'
        filename = filename[:-4]
    else:
        return
    subprocess.call('convert -alpha Remove "{}.{}" "{}.pnm"'.format(filename, extension, filename), shell=True)
    subprocess.check_call(f'potrace "{ filename }.pnm" --svg -o "{ filename }.svg"', shell=True)
    print('**************************************')
    print('**************************************')
    print('**************************************')
    print('**************************************')
    print('**************************************')
    print('**************************************')
    print('**************************************')
    print('**************************************')
    print('**************************************')
    print('**************************************')
    print('**************************************')
    print(filename +  '.svg')
    assert os.path.isfile(filename + '.svg')
    return filename + '.svg'

# sudo apt-get install potrace
if __name__ == '__main__':
    if os.path.isdir(sys.argv[1]):
        filenames = os.listdir(sys.argv[1])
        for f in filenames:
            export_to_svg(os.path.join(sys.argv[1], f))
    else:
        filename = sys.argv[1]
        export_to_svg(filename)
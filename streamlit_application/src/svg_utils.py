import subprocess
import uuid
import os
import re
import shutil
from typing import Dict, List
from bs4 import BeautifulSoup
from sklearn.cluster import KMeans
from sklearn.utils import shuffle
from matplotlib import colors
import streamlit as st

#  Todo checker
# rect, clippath clip, cls

def load_svg_string(filename):
    assert filename.endswith('.svg') or filename.endswith('.svgz')

    options = "--shorten-ids --indent=none --disable-simplify-colors --enable-comment-stripping --remove-descriptions --remove-metadata --remove-descriptive-elements --no-line-breaks --create-groups --set-precision 4"
    if filename.endswith('.svgz'):
        svgfilename = str(uuid.uuid4()) + '.svg'
        command = "scour '{}' '{}' {}".format(filename, svgfilename, options)
        subprocess.call(command, shell=True)
    else:
        svgfilename = filename

    f = open(svgfilename, 'r')
    svg_string = f.read()
    if filename.endswith('.svgz'):
        os.remove(svgfilename)
    return svg_string

def generate_preview(input_filename, output_filename, width, height, MIN_SIZE=120, expected_width=None):
    print('generate preview ', output_filename)
    if os.path.exists(output_filename):
        os.unlink(output_filename)
    tempname = str(uuid.uuid4()) + '.png'
    if expected_width is not None:
        command = 'inkscape -w {} "{}" --export-background=white -o "{}"'.format(expected_width, input_filename, tempname)
        new_width = expected_width
        new_height = new_width * height / float(width)

    elif width <= height:
        command = 'inkscape -w {} "{}" --export-background=white -o "{}"'.format(MIN_SIZE, input_filename, tempname)
        new_width = MIN_SIZE
        new_height = new_width * height / float(width)
    else:
        command = 'inkscape -h {} "{}" --export-background=white -o "{}"'.format(MIN_SIZE, input_filename, tempname)
        new_height = MIN_SIZE
        new_width = new_height * width / float(height)
    subprocess.run(command, shell=True)

    command = 'pngquant "{}" --output "{}"'.format(tempname, output_filename)
    if os.path.exists(output_filename):
        os.remove(output_filename)
    ex = subprocess.run(command, shell=True)
    os.remove(tempname)

    return (new_width, new_height)

def change_unit_to_px(filename, outfilename=None):
    binary = filename.endswith('svgz')
    if binary:
        temp_filename = str(uuid.uuid4()) + '.svg'
        subprocess.call('scour "{}" "{}"'.format(filename, temp_filename), shell=True)

    if outfilename is None:
        outfilename = filename

    with open(filename if not binary else temp_filename, 'r') as f:
        txt = f.read()
    txt = re.sub('pt', '', txt)
    txt = re.sub('mm', '', txt)
    
    if binary:
        with open(temp_filename, 'w') as f:
            f.write(txt)
        subprocess.call('scour "{}" "{}"'.format(temp_filename, outfilename), shell=True)
        os.remove(temp_filename)
        print("...done")
    else:
        with open(outfilename, 'w') as f:
            f.write(txt)

def get_size(filename):
    binary = filename.endswith('svgz')
    temp_filename = str(uuid.uuid4()) + '.svg'
    if binary:
        subprocess.call('scour "{}" "{}"'.format(filename, temp_filename), shell=True)
    change_unit_to_px(filename, outfilename=temp_filename)
    with open(temp_filename, 'r') as f:
        txt = f.read()
    start = txt.find('viewBox="')
    start += len('viewBox="')
    end = start + txt[start:].find('"')

    viewbox = [float(x) for x in txt[start:end].split(' ')]
    

    command_get_width = 'inkscape "{}" -W'.format(temp_filename)
    width = float(subprocess.run(command_get_width, shell=True, stdout=subprocess.PIPE).stdout.decode('utf8'))

    command_get_height = 'inkscape "{}" -H'.format(temp_filename)
    height = float(subprocess.run(command_get_height, shell=True, stdout=subprocess.PIPE).stdout.decode('utf8'))
    
    os.remove(temp_filename)
    return (viewbox[2], viewbox[3])


def fit_to_drawing(filename):
    command_fit_to_drawing = f'inkscape --batch-process --actions="file-open:{ filename };select-all;fit-canvas-to-selection;export-filename:{ filename };export-do;file-close"'
    subprocess.run(command_fit_to_drawing, shell=True, stdout=subprocess.PIPE)
    change_unit_to_px(filename)


def compress_with_scour(filename):
    if not os.path.exists('tmp_scour'):
        os.mkdir('tmp_scour')
    extension = 'svgz' if filename.endswith('svgz') else 'svg'
    options = "--shorten-ids --indent=none --disable-simplify-colors --enable-comment-stripping --enable-id-stripping --remove-descriptions --remove-metadata --remove-descriptive-elements --no-line-breaks --create-groups --set-precision 4"
    tempfile_path = os.path.join('tmp_scour', str(uuid.uuid4()) + '.' + extension)
    shutil.move(filename, tempfile_path)
    command = "scour '{}' '{}' {}".format(tempfile_path, filename, options)
    subprocess.run(command, shell=True)
    os.remove(tempfile_path)

# def get_svg_group(svg_filename: str):


def merge_svgs(svg_filenames: List[str], output_filename: str):
    assert len(svg_filenames) > 0
    with open(svg_filenames[0], 'r') as f:
        txt = f.read()
    merged_soup = BeautifulSoup(txt, 'xml')
    
    for svg_filename in svg_filenames[1:]:
        with open(svg_filename, 'r') as f:
            txt = f.read()

        soup = BeautifulSoup(txt, 'xml')
        svg_tag = soup.find('svg')
        for child in svg_tag.findChildren(recursive=False):
            merged_soup.find('svg').append(child)
    
    with open(output_filename, 'w') as f:
        f.write(str(merged_soup))

# Todo utiliser un décorateur pour gérer les .svgz / svg
def change_color(filename: str, colors: Dict[str, str]):
    path = filename
    if 'polychromatic' not in filename:
        path = os.path.join('tmp', filename)
    # if path

    assert filename.endswith('.svg') # temporary

    f = open(path, 'r')
    svg_string = f.read()
    f.close()

    for original_color in colors.keys():
        svg_string = re.sub(original_color, 'CC' + original_color[1:], svg_string)

    for original_color, new_color in colors.items():
        svg_string = re.sub('CC' + original_color[1:], new_color, svg_string)
    
    f = open(path, 'w')
    f.write(svg_string)
    f.close()


def format_and_detect_colors(filename, binary):
    options = "--shorten-ids --indent=none --disable-simplify-colors --enable-comment-stripping --remove-descriptions --remove-metadata --remove-descriptive-elements --no-line-breaks --create-groups --set-precision 4"
    if binary:
        svgfilename = str(uuid.uuid4()) + '.svg'
        command = "scour '{}' '{}' {}".format(filename, svgfilename, options)
        subprocess.call(command, shell=True)
    else:
        svgfilename = filename
    # todo handle binaries
    # todo handle unwanted colors '#ffffff', '#666666']
    regex = r'#([0-9a-fA-F]{6})'

    f = open(svgfilename, 'r')
    text = f.read()
    f.close()

    text = re.sub(r'pagecolor="#([a-f]|\d)*"', '', text)
    text = re.sub(r'bordercolor="#([a-f]|\d)*"', '', text)

    # Todo handle 3 chars colors
    text = re.sub(r'#fff;', '#ffffff', text)

    result = re.findall(regex, text)

    colors = list(set(list(result)))
    for color in colors:
        lowerCaseColor = color.lower()
        if color == lowerCaseColor:
            continue
        text = re.sub(color, lowerCaseColor, text)

    with open(svgfilename, 'w') as f:
        f.write(text)
    
    if binary:
        command = "scour '{}' '{}' {}".format(svgfilename, filename, options)
        subprocess.call(command, shell=True)

    text = re.sub(r'pagecolor="#([a-f]|\d)*"', '', text)
    text = re.sub(r'bordercolor="#([a-f]|\d)*"', '', text)

    result = re.findall(regex, text)

    colors = list(set(list(result)))
    colors = list(set([c.lower() for c in colors]))

    if binary:
        os.remove(svgfilename)
    return colors



def restrict_colors(html_colors, nb_colors):

    rgb_colors = [colors.hex2color(html) for html in html_colors]
    kmeans = KMeans(n_clusters=nb_colors, random_state=0).fit(
            rgb_colors)
    labels = kmeans.predict(rgb_colors)


    clusters = kmeans.cluster_centers_[labels]

    return {
        html_colors[k]: colors.to_hex(clusters[k])
        for k in range(len(html_colors))
    }
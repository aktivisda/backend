
import shutil
import sys
import os
import json
import subprocess
import uuid
from bs4 import BeautifulSoup
import shutil
import cv2
import numpy as np

if __name__ == '__main__':
    import convert_to_svg
else:
    from . import convert_to_svg

if not '../src' in sys.path:
    sys.path.insert(0, '.')

from src import convert_to_svg
from src import hitpath as hitpath_utils

style = 'fill:#000000;stroke:#000000;stroke-width:0.75px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;fill-opacity:1;opacity:1'

def read_svg(svg_filename):
    if svg_filename.endswith('.svgz'):
        new_svgfilename = str('tmp/') + str(uuid.uuid4()) + '.svg'
        command = "scour {} {}".format(svg_filename, new_svgfilename)
        subprocess.call(command, shell=True)

        with open(new_svgfilename, 'r') as f:
            return_value = f.read()
        return return_value
    else:
        with open(svg_filename, 'r') as f:
            return f.read()

def hitpath_to_png(svg_string, hitpath):
    soup = BeautifulSoup(svg_string, 'xml')
    
    path = '<svg:path d="{}" style="{}">'.format(hitpath, style)
    svgpath = BeautifulSoup(path, 'html.parser' )
    
    soup.svg.clear()
    soup.svg.append(svgpath)
    txt = soup.prettify()
    txt = txt.replace('svg:', '')

    tmp_filename = os.path.join('tmp', 'hitpath.svg')
    with open(tmp_filename, 'w') as f:
        f.write(txt)
    
    command = f'inkscape "tmp/hitpath.svg" --export-background=white -o "tmp/hitpath.png"'
    subprocess.call(command, shell=True, stdout=subprocess.DEVNULL)
    return 'tmp/hitpath.png'

def compute_hull(hitpath_png):
    img = cv2.imread(hitpath_png, cv2.IMREAD_UNCHANGED)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    stack = []

    (old_w, old_h) = img.shape
    if old_w > 400:
        scale = round(100*400./float(old_w), 3)
        command = f'convert {hitpath_png} -resize {scale}% {hitpath_png}'
        subprocess.call(command, shell=True, stdout=subprocess.DEVNULL)

        img = cv2.imread(hitpath_png, cv2.IMREAD_UNCHANGED)
        # img = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    w, h, = img.shape

    visited = 255*np.ones((w, h))
    def _append(i, j, stack, img, w, h, visited):
        if i < 0 or j < 0 or i >= w or j >= h:
            return
        
        if visited[i, j] == 0:
            return

        if img[i, j] < 254:
            return

        visited[i, j] = 0
        stack.append((i, j))

    def _append_neighbors(i, j, stack, img, w, h, visited):
        _append(i-1, j, stack, img, w, h, visited)
        _append(i+1, j, stack, img, w, h, visited)
        _append(i-1, j-1, stack, img, w, h, visited)
        _append(i+1, j-1, stack, img, w, h, visited)
        _append(i-1, j+1, stack, img, w, h, visited)
        _append(i+1, j+1, stack, img, w, h, visited)
    
    def _append_bounds(i, ii, j, jj, stack, img, w, h, visited):
        for ix in range(i, ii):

            _append(ix, j, stack, img, w, h, visited)
            _append(ix, jj-1, stack, img, w, h, visited)

        for jx in range(j, jj):
            _append(i, jx, stack, img, w, h, visited)
            _append(ii-1, jx, stack, img, w, h, visited)
    
    _append_bounds(0, w, 0, h, stack, img, w, h, visited)
    img = np.array(img)

    k = 0
    while len(stack) > 0:
        k += 1
        top = stack[0]
        stack.pop(0)
        (i, j) = top
        _append_neighbors(i, j, stack, img, w, h, visited)

    visited = 255*np.ones((w, h)) - visited
    cv2.imwrite('tmp/test-final.png', visited)
    convert_to_svg.export_to_svg('tmp/test-final.png')
    hitpath_utils.apply_transforms('tmp/test-final.svg')
    hitpath_utils.resize_svg('tmp/test-final.svg', old_w/w)
    hitpath_utils.apply_transforms('tmp/test-final.svg')

    path = hitpath_utils.extract_path('tmp/test-final.svg')
    return path


if __name__ == '__main__':

    
    if len(sys.argv) != 4:
        print('Usage: python3 hitpath_to_hull.py <path_to_folder> <path_to_json> <symbol_id>')
        # python3 scripts/test_hitforms.py src/assets/local/symbols/ src/assets/local/data/symbols.json 
        sys.exit(0)

    symbols_dir = sys.argv[1]
    json_filename = sys.argv[2]
    symbol_id = sys.argv[3]
    
    symbols = [x for x in os.listdir(symbols_dir) if x.endswith('svgz') or x.endswith('svg')]
    symbols.sort()

    f = open(json_filename)
    symbols_data = json.load(f)
    f.close()

    tmp_dir = 'tmp'
    if os.path.exists(tmp_dir):
        shutil.rmtree(tmp_dir)
    os.mkdir(tmp_dir)


    for k in range(len(symbols_data)):

        symbol_data = symbols_data[k]
        filename = symbol_data['filename']
        print(f'> { filename }')
        if 'backgroundHull' in symbol_data.keys():
            print('\t Already done')
            continue
        if 'hitform' not in symbol_data.keys():
            continue        

        hitpath_png = hitpath_to_png(svg_string=read_svg(os.path.join(symbols_dir, filename)), hitpath=symbol_data['hitform'])
        symbols_data[k]['backgroundHull'] = compute_hull(hitpath_png)
        with open(json_filename, "w", encoding="utf-8") as f:
            json.dump(symbols_data, f, ensure_ascii=False, indent=2)

        # result = test_hitpath.add_hitpath_to_svg_string(read_svg(os.path.join(symbols_dir, filename)), hitpath=symbols_data[k]['backgroundHull'])
        # with open('tmp/' + symbol_data['id'] + '.svg', 'w') as ff:
        #     ff.write(result)
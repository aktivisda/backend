import base64
import streamlit as st

def render_svg(svg, width='165px'):
    """Renders the given svg string."""
    b64 = base64.b64encode(svg.encode('utf-8')).decode("utf-8")
    html = f'<img style="width:{width}" src="data:image/svg+xml;base64,{b64}"/>'
    st.write(html, unsafe_allow_html=True)

import streamlit as st
from . import svg_utils
from . import hitpath
import datetime
import os

@st.experimental_memo(suppress_st_warning=True)
def compute_symbol_data(svg_filename, is_background=False):
    path = os.path.join('tmp', svg_filename)
    nb_steps = 7
    progress_bar = st.progress(0.)
    

    progress_bar.progress(1./nb_steps)

    svg_utils.compress_with_scour(path)
    progress_bar.progress(2./nb_steps)
    
    if not is_background:
        st.write("Fit to drawing")
        svg_utils.fit_to_drawing(path)
        svg_utils.change_unit_to_px(path)

    if not is_background:
        sym_hitpath, _, _ = hitpath.compute_hitpath(path, hitpath_directory=os.path.join('tmp', 'hitpaths'))
    else:
        sym_hitpath = ''
    progress_bar.progress(3./nb_steps)

    binary = svg_filename.endswith('svgz')
    symbol_filename_without_extension = svg_filename[:-5] if binary else svg_filename[:-4]

    progress_bar.progress(4./nb_steps)

    (width, height) = svg_utils.get_size(path)
    progress_bar.progress(5./nb_steps)

    preview_path = os.path.join('tmp', 'previews', symbol_filename_without_extension + '.png')
    svg_utils.generate_preview(path, preview_path, width, height)
    progress_bar.progress(6./nb_steps)

    colors = ["#" + c for c in svg_utils.format_and_detect_colors(path, binary)]

    progress_bar.progress(7./nb_steps)

    today = datetime.datetime.today().strftime('%Y-%m-%dT%H:%M:%S')
    symbol_data = dict(
        id=symbol_filename_without_extension, 
        creation_date=today,
        filename=svg_filename, 
        preview="previews/" + symbol_filename_without_extension + '.png',
        tags='',
        width=width, 
        height=height, 
        hitform=sym_hitpath,
        colors={ c:c for c in colors},
    )
    return symbol_data
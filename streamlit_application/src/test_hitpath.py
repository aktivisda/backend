import shutil
import sys
import os
import json
import subprocess
import uuid
from bs4 import BeautifulSoup
import shutil

style_hitpath = 'fill:#ffff00;stroke:#000000;stroke-width:0.75px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;fill-opacity:1;opacity:0.29881376'
style_hull = 'fill:#0000ff;stroke:#000000;stroke-width:0.75px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1;fill-opacity:1;opacity:0.29881376'

def to_svgfilename(filename):

    if filename.endswith('svgz'):
        svgfilename = str(uuid.uuid4()) + '.svg'
        command = "scour {} {}".format(filename, svgfilename)
        subprocess.call(command, shell=True)
        return svgfilename
    else:
        return filename

def add_hitpath_to_svg_string(svg_string, hitpath, hull):
    soup = BeautifulSoup(svg_string, 'xml')
    if hull is not None:
        path = '<svg:path d="{}" style="{}">'.format(hull, style_hull) if hull is not None else ''
        svgpath = BeautifulSoup(path, 'html.parser' )
        soup.svg.append(svgpath)
    

    path = '<svg:path d="{}" style="{}">'.format(hitpath, style_hitpath)
    svgpath = BeautifulSoup(path, 'html.parser' )
    soup.svg.append(svgpath)
    txt = soup.prettify()
    txt = txt.replace('svg:', '')
    return txt


if __name__ == '__main__':

    tmp_dir = 'tmp'
    if os.path.exists(tmp_dir):
        shutil.rmtree(tmp_dir)
    os.mkdir(tmp_dir)
    # python3 scripts/test_hitforms.py src/assets/local/symbols/ src/assets/local/data/symbols.json 
    
    if len(sys.argv) != 3:
        print('Usage: python3 test_hitforms.pyt <path_to_folder> <path_to_json>')
        sys.exit(0)

    symbols_dir = sys.argv[1]
    json_filename = sys.argv[2]

    symbols = [x for x in os.listdir(symbols_dir) if x.endswith('svgz') or x.endswith('svg')]
    symbols.sort()

    f = open(json_filename)
    symbols_data = json.load(f)

    for symbol_data in symbols_data:
        filename = symbol_data['filename']
        if 'hitform' not in symbol_data.keys():
            continue
        
        svgfilename = to_svgfilename(os.path.join(symbols_dir, filename))
        print(svgfilename)

        filename_not_binary = filename[:-1] if filename.endswith('z') else filename
        f = open(os.path.join(tmp_dir, filename_not_binary), 'w')
        print(svgfilename)


        f.write(add_hitpath_to_svg_string(svg_string=open(svgfilename, 'r').read(), hitpath=symbol_data['hitform'], hull=symbol_data['backgroundHull'] if 'backgroundHull' in symbol_data.keys() else None))
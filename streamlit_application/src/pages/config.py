import os
import streamlit as st
from .. import data_utils

import uuid
import subprocess

AKTIVISDA_DIRECTORY = os.getenv('AKTIVISDA_DIRECTORY')


def favicon_selection(data):

    st.write('### Favicon')
    col1, col2 = st.columns(2)
    col1.image(data.config()['favicon_png'])

    new_favicon_ico = col2.file_uploader('Please select a favicon from your computer', type=['ico'], accept_multiple_files=False)
    if new_favicon_ico:
        data.update_favicon(new_favicon_ico)
        
def buefy(data):
    st.write('### Style')
    columns = st.columns(4)
    primary_color = columns[0].color_picker('Primary color', data.config()['style']['primary-color'])
    primary_text_color = columns[1].color_picker('Primary text color', data.config()['style']['primary-text-color'])
    secondary_color = columns[2].color_picker('Secondary color', data.config()['style']['secondary-color'])
    secondary_text_color = columns[3].color_picker('Secondary text color', data.config()['style']['secondary-text-color'])

    if primary_color != data.config()['style']['primary-color']:
        st.write("Primary color changed !")

    if st.button('Save new colors'):
        data.update_buefy({ 'primary-color': primary_color, 'primary-text-color': primary_text_color, 'secondary-color':secondary_color, 'secondary-text-color': secondary_text_color})
        st.success('Colors updated')
    
def run(data: data_utils.Data):
    favicon_selection(data)
    buefy(data)


import os
import streamlit as st
from .. import data_utils
from .. import utils

import streamlit.components.v1 as v1components


AKTIVISDA_DIRECTORY = os.getenv('AKTIVISDA_DIRECTORY')

FORMATS_EXPLANATIONS = {
    'woff': {
        'title': 'WOFF',
        'help': '"Web Open Font Format (WOFF) is basically OTF or TTF with metadata and compression supported by all major browsers. It was created to live on the web. It is the result of collaboration by the Mozilla Foundation, Microsoft, and Opera Software." ([Source](https://creativemarket.com/blog/the-missing-guide-to-font-formats)).',
        'necessary': True
    },
    'otf': {
        'title': 'OTF/TTF',
        'help': 'TrueType Font (TTF) and OpenType Font (OTF) can be used by Macintosh and Windows ([Source](https://creativemarket.com/blog/the-missing-guide-to-font-formats)). Most of web browsers support OTF and TTF.',
        'necessary': True
    },
    'ttf': {
        'title': 'OTF/TTF',
        'help': 'TrueType Font (TTF) and OpenType Font (OTF) can be used by Macintosh and Windows ([Source](https://creativemarket.com/blog/the-missing-guide-to-font-formats)). Most of web browsers support OTF and TTF.',
        'necessary': True
    },
    'woff2': {
        'title': 'Woff2',
        'help': 'Woff2 is the next generation of woff. It\'s recommended but not necessary.',
        'necessary': False
    }
}

def font_format_file(formats, font_format, font_name):
    st.write(f'#### { font_format }')
    if font_format in formats.keys():
        st.success(f'✅ You have an .{ font_format } version !')
    elif FORMATS_EXPLANATIONS[font_format]['necessary']:
        st.error(
                f'You should add an .{ font_format } version of {font_name }')
    else:
        st.warning(
            f'You should add an .{ font_format } version of {font_name }')
    font_path = st.file_uploader('Select font from your computer', accept_multiple_files=False,
                    type=font_format, key=f'font-upload-{ font_format}-{font_name }')
    st.write('---')
    return font_path

def save_font_file(uploaded_file, directory, font_name, font_format):
    f = open(os.path.join(AKTIVISDA_DIRECTORY, 'static', 'fonts', directory, font_name + '.' + font_format), 'wb')
    f.write(uploaded_file.read())
    f.close()
    return font_name + '.' + font_format

def run(data: data_utils.Data):
    fonts_version = data.fonts['version']
    fonts = data.fonts['fonts']

    if len(fonts) == 0:
        st.error('Please add your first font.')
        return
    
    if st.sidebar.button('🆕 Add a new font'):
        data.fonts['fonts'].append({
            "css": {
                "font-style": "normal",
                "font-weight": "normal",
                "font-display": None,
                "unicode-range": None
            },
            "fontName": "dummy_font",
            "formats": {
            },
            "directory": ".",
            "name": "Dummy Font",
            "loaded": False,
            "italic": False,
            "bold": False,
            "alwaysLoaded": True,
            "default": True
        })
        data.save()

    font_index = st.sidebar.selectbox('Font', list(range(0, len(fonts))), len(fonts) - 1, format_func=lambda k: fonts[k]['fontName'])
    st.session_state['font_index'] = font_index

    font = fonts[font_index]

    font['fontName'] = st.text_input(label='Font Name', value=font['fontName'])
    font['name'] = st.text_input(label='Display name', value=font['name'])
    #font['directory'] = st.text_input(label='Directory', value=font['directory'])

    font_ttf = font_format_file(font['formats'], 'ttf', font['fontName'])
    if font_ttf is not None:
        font['formats']['ttf'] = save_font_file(font_ttf, font['directory'], font['fontName'], 'ttf')

    font_otf = font_format_file(font['formats'], 'otf', font['fontName'])
    if font_otf is not None:
        font['formats']['otf'] = save_font_file(font_otf, font['directory'], font['fontName'], 'otf')

    font_woff = font_format_file(font['formats'], 'woff', font['fontName'])
    if font_woff is not None:
        font['formats']['woff'] = save_font_file(font_woff, font['directory'], font['fontName'], 'woff')

    font_woff2 = font_format_file(font['formats'], 'woff2', font['fontName'])
    if font_woff2 is not None:
        font['formats']['woff2'] = save_font_file(font_woff2, font['directory'], font['fontName'], 'woff2')

    cols = st.columns(2)
    if cols[0].button(f'💾 Save',
            key=f'font-save-{ font["fontName"] }'
        ):
        data.fonts['fonts'][font_index] = font
        data.save()
    st.write('---')
    st.write('See your fonts in action: http://localhost:8080/en/fonts')
    v1components.iframe("http://localhost:8080/en/fonts", scrolling=True, height=300)
import streamlit as st

from . import data_utils
from . import router_utils

def run(data: data_utils.Data):

    templates = data.templates
    template_ids = [template['id'] for template in templates]

    template_id = router_utils.get_url_param_or_none('template')
    template_id_index = template_ids.index(template_id) if template_id is not None and template_id in template_ids else len(template_ids) - 1
    template_id = st.selectbox('Select the template to update', template_ids, template_id_index)
    router_utils.set_url_param('template', template_id)

    template = templates[template_id_index]
    st.write(template)

    if st.button('Duplicate'):
        new_template_id = data.duplicate_template(template['id'])
        st.info(f'Template { template["id"] } successfully duplicated (see { new_template_id })')
    return
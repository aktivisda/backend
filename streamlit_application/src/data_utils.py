import os
import json
import uuid
from typing import Dict, List, Any
import shutil
import copy
import enum
import subprocess
import re

SYMBOLS_DATA_FILENAME = os.getenv('SYMBOLS_DATA_FILENAME')
BACKGROUNDS_DATA_FILENAME = os.getenv('BACKGROUNDS_DATA_FILENAME')
TEMPLATES_DATA_FILENAME = os.getenv('TEMPLATES_DATA_FILENAME')
FONTS_DATA_FILENAME = os.getenv('FONTS_DATA_FILENAME')
FONTS_CSS_FILENAME = os.getenv('FONTS_CSS_FILENAME')
I18N_LOCAL_FILES = os.getenv('I18N_LOCAL_FILES')
AKTIVISDA_DIRECTORY = os.getenv('AKTIVISDA_DIRECTORY')

def load_json(filename: str):
    with open(filename) as file:
        return json.load(file)

def save_json(filename: str, data):
    with open(filename, 'w') as save_file:
        json.dump(data, save_file, ensure_ascii=False, indent=2)

def symbol_or_background_preview_filename(preview, str_mode: str, is_temporary=False):
    if is_temporary:
        return os.path.join('tmp', preview)
    else:
        return os.path.join(AKTIVISDA_DIRECTORY, 'static', str_mode + 's', preview)
        
class SymbolType(enum.Enum):
    BACKGROUND=0,
    SYMBOL=1

    @staticmethod
    def to_string(t):
        if t == SymbolType.BACKGROUND:
            return 'background'
        return 'symbol'

DEFAULT_LANGUAGE = 'fr'

class Data:

    tags: Dict[str, Dict[str, str]]
    symbols: List[Dict]
    langs: List[str] # [en, fr, etc.]
    locales: Dict[str, Any]
    templates: List[Dict[str, str]]
    config: Dict[str, Any]

    def __init__(self):
        self._load_langs()
        self._load_locales()
        self._load_backgrounds()
        self._load_symbols()
        self._load_tags()
        self._load_templates()
        self._load_config()
        self._load_fonts()

    def save(self):
        for lang in self.langs:
            self.locales[lang]['tags'] = {}

        for tag, translations in self.tags.items():
            at_least_one_translation = False
            for lang, translation in translations.items():
                if translation != "":
                    at_least_one_translation = True
                    break

            for lang, translation in translations.items():
                # TODO default language

                if translation == "" and lang != DEFAULT_LANGUAGE:
                    continue
                self.locales[lang]['tags'][tag] = translation


        for lang in self.langs:
            save_json(os.path.join(AKTIVISDA_DIRECTORY, I18N_LOCAL_FILES, lang + '.json'), self.locales[lang])
        
        save_json(os.path.join(AKTIVISDA_DIRECTORY, BACKGROUNDS_DATA_FILENAME), self._backgrounds)
        save_json(os.path.join(AKTIVISDA_DIRECTORY, SYMBOLS_DATA_FILENAME), self._symbols)
        save_json(os.path.join(AKTIVISDA_DIRECTORY, TEMPLATES_DATA_FILENAME), self.templates)
        save_json(os.path.join(AKTIVISDA_DIRECTORY, FONTS_DATA_FILENAME), self.fonts)

    def duplicate_template(self, template_id: str) -> str:
        template = [t for t in self.templates if t['id'] == template_id]
        if len(template) != 1:
            raise ValueError(f'Error {len(template)} templates with id { template_id } found.')
        template = template[0]
        new_template_id = str(uuid.uuid4())
        new_template = copy.copy(template)
        new_template['id'] = new_template_id
        new_template['filename'] = new_template_id + '.json'
        new_template['preview'] = f'previews/{new_template_id}.png'

        shutil.copy(
            os.path.join(AKTIVISDA_DIRECTORY, 'static', 'templates', template['preview']),
            os.path.join(AKTIVISDA_DIRECTORY, 'static', 'templates', new_template['preview'])
        )

        shutil.copy(
            os.path.join(AKTIVISDA_DIRECTORY, 'local', 'templates', template['filename']),
            os.path.join(AKTIVISDA_DIRECTORY, 'local', 'templates', new_template['filename'])
        )
        self.templates.append(new_template)

        self._save()
        return new_template_id

    def update_or_add_symbol_or_background(self, symbol: Dict, mode: SymbolType):
        if mode == SymbolType.BACKGROUND:
            for k in range(len(self._backgrounds['backgrounds'])):
                if self._backgrounds['backgrounds'][k]['id'] == symbol['id']:
                    self._backgrounds['backgrounds'][k] = symbol
                    break
            else:
                self._backgrounds['backgrounds'].append(symbol)

        elif mode == SymbolType.SYMBOL:
            for k in range(len(self._symbols['symbols'])):
                if self._symbols['symbols'][k]['id'] == symbol['id']:
                    self._symbols['symbols'][k] = symbol
                    break
            else:
                self._symbols['symbols'].append(symbol)

    # Todo check if used in templates
    def remove_symbol_or_background(self, symbol_id: str, mode: SymbolType):
        if mode == SymbolType.BACKGROUND:
            for k in range(len(self._backgrounds['backgrounds'])):
                if self._backgrounds['backgrounds'][k]['id'] == symbol_id:
                    if os.path.exists(symbol_or_background_preview_filename(self._backgrounds['backgrounds'][k]['preview'], 'background')):
                        os.remove(symbol_or_background_preview_filename(self._backgrounds['backgrounds'][k]['preview'], 'background'))
                    if os.path.exists(symbol_or_background_preview_filename(self._backgrounds['backgrounds'][k]['filename'], 'background')):
                        os.remove(symbol_or_background_preview_filename(self._backgrounds['backgrounds'][k]['filename'], 'background'))
                    del self._backgrounds['backgrounds'][k]
                    return
        elif mode == SymbolType.SYMBOL:
            for k in range(len(self._symbols['symbols'])):
                if self._symbols['symbols'][k]['id'] == symbol_id:
                    if os.path.exists(symbol_or_background_preview_filename(self._symbols['symbols'][k]['preview'], 'symbol')):
                        os.remove(symbol_or_background_preview_filename(self._symbols['symbols'][k]['preview'], 'symbol'))
                    if os.path.exists(symbol_or_background_preview_filename(self._symbols['symbols'][k]['filename'], 'symbol')):
                        os.remove(symbol_or_background_preview_filename(self._symbols['symbols'][k]['filename'], 'symbol'))
                    del self._symbols['symbols'][k]
                    return

    def update_tag(self, tag: str, translations):
        self.tags[tag] = translations

    def remove_tag(self, tag_key:str):
        del self.tags[tag_key]

    def create_tag(self, key):
        if ',' in key:
            raise ValueError(f'Tag "{key}" should not contain comma.')
        if key in self.tags.keys():
            raise Exception('Duplicated key')
        self.tags[key] = { lang: '' for lang in self.langs }
        for lang in self.langs:
            self.locales[lang]['tags'][key] = ''

    def symbols_or_backgrounds(self, mode: SymbolType):
        if mode == SymbolType.BACKGROUND:
            return self._backgrounds['backgrounds']
        else:
            return self._symbols['symbols']

    def symbols_with_tag(self, tag):
        return [s for s in self._symbols['symbols'] if Data.has_tag(s, tag)]

    def backgrounds_with_tag(self, tag):
        return [b for b in self._backgrounds['backgrounds'] if Data.has_tag(b, tag)]

    @staticmethod
    def has_tag(symbol: Dict, tag: str):
        if tag == 'all': return True
        tags = symbol['tags'].split(',')
        return tag in tags

    def _load_locales(self):
        assert self.langs
        self.locales = {}
        for lang in self.langs:
            self.locales[lang] = load_json(os.path.join(AKTIVISDA_DIRECTORY, I18N_LOCAL_FILES, lang + '.json'))

    def _load_langs(self):
        # todo use localconfig and langs.json
        langs = os.listdir(os.path.join(AKTIVISDA_DIRECTORY, I18N_LOCAL_FILES))
        lang_files = [x[:-len('.json')] for x in langs]   
        self.langs = ['fr', 'de', 'en']
            
    def _load_tags(self):
        assert self._backgrounds['backgrounds']
        assert self.langs
        assert self._symbols['symbols']
        self.tags = {}

        for symbol in self._symbols['symbols']:
            for tag in symbol['tags'].split(','):
                if not tag: continue
                if not tag in self.tags.keys():
                    self.tags[tag] = { lang: '' for lang in self.langs }

        for background in self._backgrounds['backgrounds']:
            for tag in background['tags'].split(','):
                if not tag: continue
                if not tag in self.tags.keys():
                    self.tags[tag] = { lang: '' for lang in self.langs }

        for lang, locale in self.locales.items():
            for tag, translation in locale['tags'].items():
                if not tag: continue
                if not tag in self.tags:
                    self.tags[tag] = { ll: '' for ll in self.langs}
                self.tags[tag][lang] = translation
    
    def _load_backgrounds(self):
        self._backgrounds = load_json(os.path.join(AKTIVISDA_DIRECTORY, BACKGROUNDS_DATA_FILENAME))

    def _load_symbols(self):
        self._symbols = load_json(os.path.join(AKTIVISDA_DIRECTORY, SYMBOLS_DATA_FILENAME))

    def _load_templates(self):
        self.templates = load_json(os.path.join(AKTIVISDA_DIRECTORY,TEMPLATES_DATA_FILENAME))

    def _load_favicon_png(self):
        favicon_ico = os.path.join(AKTIVISDA_DIRECTORY, 'favicon.ico')
        self._config['favicon_png'] = os.path.join('tmp', 'favicon.png')
        cmd = f'convert { favicon_ico } { self._config["favicon_png"] }'
        subprocess.call(cmd, shell=True)

    def _load_buefy(self):
        def _load_color(label, regex):
            compiled_regex = re.compile(regex)
            if compiled_regex.search(content):
                self._config['style'][label] = [m.groups() for m in compiled_regex.finditer(content)][0][0]
            else:
                self._config['style'][label] = '#000000'
                    
        self._config['style'] = {}
        buefy_path = os.path.join(AKTIVISDA_DIRECTORY, 'local', 'buefy.scss')

        with open(buefy_path, 'r') as f:
            content = f.read()
    
        _load_color('primary-color', r'--primary-color: (#.{6});')
        _load_color('secondary-color', r'--secondary-color: (#.{6});')
        _load_color('primary-text-color', r'--primary-text-color: (#.{6});')
        _load_color('secondary-text-color', r'--secondary-text-color: (#.{6});')
        return

    def update_buefy(self, style):
        def _update_color(content, regex, subst):
            content = re.sub(regex, subst, content, 0, re.MULTILINE)
            return content

        buefy_path = os.path.join(AKTIVISDA_DIRECTORY, 'local', 'buefy.scss')
        with open(buefy_path, 'r') as f:
            content = f.read()

        content = _update_color(content, r'\$primary: (#.{6});', f'$primary: { style["primary-color"] };')
        content = _update_color(content, r'\$twitter: (#.{6});', f'$twitter: { style["primary-color"] };')
        content = _update_color(content, r'--primary-color: (#.{6});', f'--primary-color: { style["primary-color"] };')
        content = _update_color(content, r'--secondary-color: (#.{6});', f'--secondary-color: { style["secondary-color"] };')
        content = _update_color(content, r'--primary-text-color: (#.{6});', f'--primary-text-color: { style["primary-text-color"] };')
        content = _update_color(content, r'--secondary-text-color: (#.{6});', f'--secondary-text-color: { style["secondary-text-color"] };')

        with open(buefy_path, 'w') as f:
            f.write(content)

        self._load_buefy()

    def _load_config(self):
        self._config = {}

        self._load_favicon_png()
        self._load_buefy()

    def update_favicon(self, new_favicon_ico):
        favicon_path = os.path.join(AKTIVISDA_DIRECTORY, 'favicon.ico')
        f = open(favicon_path, 'wb')
        f.write(new_favicon_ico.read())
        f.close()
        self._load_favicon_png()

    def config(self):
        return self._config

    def remove_font(self, font_family, font_index):
        # Todo check si la font n'est pas utilisée dans un template

        del self.fonts[font_family][font_index]

        if len(self.fonts[font_family]) == 0:
            del self.fonts[font_family]
        self._save_and_loadfonts()
    

    def _load_fonts(self):
        self.fonts = load_json(os.path.join(AKTIVISDA_DIRECTORY, FONTS_DATA_FILENAME))
        return
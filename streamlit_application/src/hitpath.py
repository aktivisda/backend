import numpy as np
from PIL import Image
import os
from . import svg_utils
import sys
import random
from . import convert_to_svg
import subprocess
from bs4 import BeautifulSoup
import json
import uuid
import re
from . import apply_transform

random.seed()

def draw_hitpath(hitpaths_dir, pngfilename):
    print('.................. ')
    print('.................. ')
    print('.................. ')
    print('.................. ')
    print('.................. ')
    print('.................. ')
    print('.................. ')
    print('.................. ')
    print('.................. ')
    print('.................. ')
    print('.................. ')
    print('.................. ')
    print('.................. ')
    print('.................. ')
    print('.................. ')
    print('.................. ')
    print('png filename ', pngfilename)
    basename = os.path.basename(pngfilename)
    (basename, extension) = os.path.splitext(basename)
    # Avoir une image avec fond blanc
    img = Image.open(pngfilename).convert('L')
    array = np.array(img)

    def dist(black1, black2):
        return max(abs(black1[0] - black2[0]), abs(black1[1] - black2[1]))

    def is_black_surrounded(i, j, array):
        if i > 0 and array[i - 1, j] > 0: return False
        if j > 0 and array[i, j - 1] > 0: return False
        if i < array.shape[0] - 1 and array[i + 1, j] > 0: return False
        if j < array.shape[1] - 1 and array[i, j + 1] > 0: return False
        return True

    def compute_segment(i1, j1, i2, j2, T):
        segment = set()
        T = float(T)
        for t in range(int(T)):
            i = t/T*i1 + (T - t)/T*i2
            j = t/T*j1 + (T - t)/T*j2
            segment.add((round(i), round(j)))
        return segment
    blacks = []

    new_blacks = set()
    for i in range(array.shape[0]):
        for j in range(array.shape[1]):
            if array[i, j] != 255:
                new_blacks.add((i, j))
                array[i, j] = 0

    itr = 0

    while len(new_blacks) > 100 and itr < 8:
        blacks.extend(new_blacks)

        copy_blacks = []
        for (i, j) in blacks:
            if is_black_surrounded(i, j, array): continue
            copy_blacks.append((i, j))
        blacks = copy_blacks

        new_blacks = set()
        for i in range(len(blacks)):
            black1 = blacks[i]
            for j in range(i + 1, len(blacks)):
                black2 = blacks[j]
                if dist(black1, black2) >= 8: continue
                segment = compute_segment(black1[0], black1[1], black2[0], black2[1], 10)
                for (ii, jj) in segment:
                    if array[ii, jj] == 0: continue;
                    array[ii, jj] = 0
                    new_blacks.add((ii, jj))

        img = Image.fromarray(array)
        img.save('{}/{}-hitpath-{}.png'.format(hitpaths_dir, basename, itr))
        itr += 1
    print('------------------------')
    print('------------------------')
    print('------------------------')
    print('------------------------')
    print('------------------------')
    print('------------------------')
    print('------------------------')
    print('------------------------')
    print('------------------------')
    print('------------------------')
    print('------------------------')
    print('------------------------')
    print('basename', basename)
    img.save('{}/{}-hitpath.png'.format(hitpaths_dir, basename))

def apply_transforms(filename: str):
    apply_transform.apply_transform(filename, filename)

def convert_to_path(filename):
    command = f'inkscape --batch-process --actions="file-open:{ filename };select-all;object-to-path;export-filename:{ filename };export-do;file-close"'
    subprocess.call(command, shell=True)

def extract_path(filename: str) -> str:
    with open(filename, 'r') as f:
        txt = f.read()
    soup = BeautifulSoup(txt, 'lxml')
    paths = list(soup.find_all('svg:path')) + list(soup.find_all('path'))

    if len(paths) == 0:
        print('No paths ?!')

    elif len(paths) > 1:
        print('Too many paths...')

    merged_path = ''
    for x in paths:
        str_path = x['d']
        str_path = 'M' + str_path[1:]
        merged_path += str_path
    return merged_path

def resize_svg(filename, scale):
    f = open(filename, 'r')
    soup = BeautifulSoup(f.read(), 'xml')

    for x in soup.find_all('path'):
        x['transform'] = 'matrix({},0,0,{},0,0)'.format(str(scale), str(scale))
    f.close()
    with open(filename, 'w') as f:
        f.write(str(soup))

def convert_all_colors_in_black(svgfilename, tmp_svgfilename):
    with open(svgfilename, 'r') as source:
        text = source.read()

    black_txt = re.sub(r'#([a-f]|\d)*"', '#000000"', text)
    with open(tmp_svgfilename, 'w') as export:
        export.write(black_txt)

def compute_hitpath(svgfilename, hitpath_directory=None):
    if hitpath_directory is None:
        hitpath_directory = str(uuid.uuid4())
    if not os.path.exists(hitpath_directory):
        os.mkdir(hitpath_directory)
    print('compute hitpath for ', svgfilename)
    basename,_ = os.path.splitext(os.path.basename(svgfilename))
    convert_to_path(svgfilename)
    svg_utils.compress_with_scour(svgfilename)

    tmp_svgfilename = str(uuid.uuid4()) + '.svg'
    if svgfilename.endswith('svgz'):
        command = 'scour "{}" "{}"'.format(svgfilename, tmp_svgfilename)
        subprocess.call(command, shell=True)
        convert_all_colors_in_black(tmp_svgfilename, tmp_svgfilename)
    else:
        convert_all_colors_in_black(svgfilename, tmp_svgfilename)

    (width, height) = svg_utils.get_size(svgfilename)

    (new_width, _) = svg_utils.generate_preview(tmp_svgfilename, '{}/{}.png'.format(hitpath_directory, basename), width, height, MIN_SIZE=100)
    os.remove(tmp_svgfilename)

    draw_hitpath(hitpath_directory, hitpath_directory + '/' + basename + '.png')

    convert_to_svg.export_to_svg(hitpath_directory + '/' + basename + '-hitpath.png')
    svg_utils.change_unit_to_px(hitpath_directory + '/' + basename + '-hitpath.svg')
    apply_transforms(hitpath_directory + '/' + basename + '-hitpath.svg')
    (new_width, _) = svg_utils.get_size(hitpath_directory + '/' + basename + '-hitpath.svg')
    scale = width/new_width

    apply_transforms(hitpath_directory + '/' + basename + '-hitpath.svg')
    resize_svg(hitpath_directory + '/' + basename + '-hitpath.svg', scale)
    apply_transforms(hitpath_directory + '/' + basename + '-hitpath.svg')
    svg_utils.fit_to_drawing(hitpath_directory + '/' + basename + '-hitpath.svg')
    
    path = extract_path(hitpath_directory + '/' + basename + '-hitpath.svg')
    # shutil.rmtree(hitpaths_dir)
    return path, width, height

if __name__ == '__main__':

    if not os.path.exists('hitpaths'):
        os.mkdir('hitpaths')

    if os.path.isdir(sys.argv[1]):
        symbols_dir = sys.argv[1]
        filenames = list(os.listdir(symbols_dir))
        random.shuffle(filenames)
        for filename in filenames:
            if not filename.endswith('.svg') and not filename.endswith('.svgz'): continue
            svgfilename = os.path.join(symbols_dir, filename)
            (width, height) = svg_utils.get_size(svgfilename)
            basename,_ = os.path.splitext(os.path.basename(svgfilename))
            svg_utils.generate_preview(svgfilename, 'hitpaths/{}.png'.format(basename), width, height, MIN_SIZE=100)
            draw_hitpath('hitpaths/' + basename + '.png')
    elif sys.argv[1].endswith('.json'):
        symbols_folder = sys.argv[2]
        symbol_filename = sys.argv[1]
        f = open(symbol_filename)
        symbols_data = json.load(f)
        f.close()
        for k in range(len(symbols_data)):
            idx = len(symbols_data) - 1 - k
            data = symbols_data[idx]
            # if 'hitpath' in data.keys(): continue
            svgfilename = os.path.join(sys.argv[2], data['filename'])
            if not os.path.exists(svgfilename):
                print('skip ', data['filename'])
                continue

            path, width, height = compute_hitpath(svgfilename)
            symbols_data[idx]['width'] = width
            symbols_data[idx]['hitpath'] = path
            symbols_data[idx]['height'] = height
            with open(symbol_filename, "w", encoding="utf-8") as f:
                json.dump(symbols_data, f, ensure_ascii=False, indent=2)
    else:
        svgfilename = sys.argv[1]
        svg_utils.change_unit_to_px(svgfilename)
        print(compute_hitpath(svgfilename))
import sys
import cv2

from matplotlib import colors
import numpy as np
import shutil
import copy
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.utils import shuffle
import os
from math import sqrt
from typing import List
import base64

import streamlit as st

from . import convert_to_svg
from . import svg_utils

@st.cache
def convert_polychromatic_png(png_file: str, nb_colors: int, white_to_alpha: bool):
    tmp_path = 'tmp-polychromatic'

    # if os.path.exists(tmp_path):
    #     shutil.rmtree(tmp_path)
    
        
    colors = detect_colors_in_png(png_file, nb_colors, tmp_path, white_to_alpha)

    return {
        'quantized_image': os.path.join(tmp_path, 'quantized_image.png'),
        'colors': list(colors.keys()),
        'masks': [os.path.join(tmp_path, 'mask-' + color + '.png') for color in colors.keys()],
        'masks-original': [os.path.join(tmp_path, 'mask-' + color + '-original.png') for color in colors.keys()]
    }


@st.experimental_memo
def to_base64_html_img(png_filename: str, style='margin:10px', width=120):
    with open(png_filename, "rb") as img_file:
        b64_string = base64.b64encode(img_file.read()).decode('ascii')
        return f'<img width="{ width }" style="{ style }" src="data:image/png;base64,{ b64_string}"</img>'

def to_html_img_list(png_filenames: List[str], urls: List[str], img_style='margin: 10px', img_width=120):
    assert len(png_filenames) == len(urls)
    html_galery = '<div>'
    for k in range(len(png_filenames)):
        html_galery += f'<a href="{ urls[k] }" target="_self">'
        html_galery += to_base64_html_img(png_filenames[k], style=img_style, width=img_width)     
        html_galery += '</a>'
    html_galery += '</div>'
    return html_galery

def color_distances(html1, html2):
    # unused
    # https://en.wikipedia.org/wiki/Color_difference
    rgb1 = colors.hex2color(html1)
    rgb2 = colors.hex2color(html2)

    rgb1 = (255*rgb1[0], 255*rgb1[1], 255*rgb1[2])
    rgb2 = (255*rgb2[0], 255*rgb2[1], 255*rgb2[2])

    DR = rgb1[0] - rgb2[0]
    DG = rgb1[1] - rgb2[1]
    DB = rgb1[2] - rgb2[2]

    redmean = (rgb1[0] + rgb2[0]) /2

    if redmean < 158:
        return sqrt(2*DR**2 + 4*DG**2 + 3*DB**2)
    return sqrt(3*DR**2 + 4*DG**2 + 2*DB**2)
    # return sqrt((rgb1[0] - rgb2[0])**2 + (rgb1[1] - rgb2[1])**2 + (rgb1[2] - rgb2[2])**2)

def detect_colors_in_png(png_filename, nb_colors, out_directory, white_to_alpha=True):
    nb_colors = int(nb_colors)
    assert png_filename.endswith('.png')

    if not os.path.exists(out_directory):
        os.mkdir(out_directory)
        
    img = cv2.imread(png_filename, cv2.IMREAD_UNCHANGED)
    if len(img.shape) == 2: # gray image
        st.write(img)
        img = cv2.merge([img, img, img])
    
    w, h, d = tuple(img.shape)
    if d == 3: # rgb image (no alpha channel)
        rgba_img = cv2.cvtColor(img, cv2.COLOR_RGB2RGBA)
        if white_to_alpha:
            rgba_img[:,:,3] = 255*(1 - (img[:,:,0] > 200) * (img[:,:,1] > 200) * (img[:,:,2] > 200))
        # rgba_img[:,:,0] = img[:,:,0]
        st.write(rgba_img[:,:,3])
        img = rgba_img

    w, h, d = tuple(img.shape)
    assert d == 4 # rgba

    image_array = np.reshape(img, (w * h, d))
    image_array_no_transparency = image_array[np.where(image_array[:, 3] > 0)[0], :3]

    image_array_sample = shuffle(image_array_no_transparency, random_state=0, n_samples=min(10_000, image_array_no_transparency.shape[0]))
    kmeans = KMeans(n_clusters=nb_colors, random_state=0).fit(image_array_sample)
    labels = kmeans.predict(image_array[:, :3])
    
    output_img = copy.copy(img)
    output_img[:, :, :3] = kmeans.cluster_centers_[labels].reshape(w, h, -1)
    cv2.imwrite(os.path.join(out_directory, 'quantized_image.png'), output_img)

    def bgr_to_rgb(bgr):
        bgr = bgr / 255.
        print(bgr)
        return [max(0, bgr[2]), max(0, bgr[1]), max(0, bgr[0])] # r, g, b
    
    # detected_colors = [colors.rgb2hex(bgr_to_rgb(bgr)) for bgr in kmeans.cluster_centers_]
    print(kmeans.cluster_centers_)
    # print('detected_colors', detected_colors)
    # ['#000000', '#d2d2d2', '#b3ac00', '#1306e3']
    detected_colors = {}

    if nb_colors == 1:
        label = 0
        colored_img = img
        mask_img = img
        
        bgr = kmeans.cluster_centers_[label]
        html = colors.rgb2hex(bgr_to_rgb(bgr))

        cv2.imwrite(os.path.join(out_directory, f'mask-{html}.png'), colored_img)
        cv2.imwrite(os.path.join(out_directory, f'mask-{html}-original.png'), mask_img)
        detected_colors[html] = { 'nb_pixels': np.sum(mask_img) }
        
        return detected_colors

    for label in range(nb_colors):
        colored_img = np.zeros((w, h, 4))
        good_pixels = (labels == label).reshape(w, h, -1) * (img[:, :, 3] > 0).reshape(w, h, 1)
        colored_img[:, :, 0:1] = 0*good_pixels
        colored_img[:, :, 1:2] = 0*good_pixels
        colored_img[:, :, 2:3] = 0*good_pixels
        colored_img[:, :, 3:4] = 255*good_pixels


        bgr = kmeans.cluster_centers_[label]
        mask_img = np.zeros((w, h, 4))
        mask_img[:, :, 0:1] = bgr[0]*good_pixels
        mask_img[:, :, 1:2] = bgr[1]*good_pixels
        mask_img[:, :, 2:3] = bgr[2]*good_pixels
        mask_img[:, :, 3:4] = 255*good_pixels

        print(bgr)
        html = colors.rgb2hex(bgr_to_rgb(bgr))
        cv2.imwrite(os.path.join(out_directory, f'mask-{html}.png'), colored_img)
        cv2.imwrite(os.path.join(out_directory, f'mask-{html}-original.png'), mask_img)
        detected_colors[html] = { 'nb_pixels': np.sum(good_pixels) }
    
    return detected_colors


def convert_and_merge_to_svg(png_filenames: List[str], colors: List[str], output_path: str):
    svg_filenames = []
    for k in range(len(png_filenames)):
        png_filename = png_filenames[k]
        svg_filenames.append(convert_to_svg.export_to_svg(png_filename))
        print('/////////////////////////////////////')
        print('convert and merge to svg ', svg_filenames[-1])
        color = colors[k]
        svg_utils.change_color(svg_filenames[-1], { "#000000": color })
    
    svg_utils.merge_svgs(svg_filenames, output_path)
    


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('Usage: python3 svg_utils.py <your_image.png> <nb_colors>')
        sys.exit(1)
    
    png_filename = sys.argv[1]
    nb_colors = int(sys.argv[2])

    temp_out_directory = '17'#str(uuid.uuid4())


    detect_colors_in_png(png_filename, nb_colors, temp_out_directory)
    png_masks = [mask for mask in sorted(os.listdir(temp_out_directory)) if mask.startswith('mask') and mask.endswith('.png')]
    print(png_masks)
    for png_mask in png_masks:
        color = os.path.basename(png_mask)[len('mask-'):-len('.png')]
        print('color !', color)
        path = os.path.join(temp_out_directory, png_mask)
        svg_mask = convert_to_svg.export_to_svg(path)
        print('svg mask ', svg_mask)
        svg_utils.change_color(svg_mask, { '#000000': color })

        # print(svg_masks)
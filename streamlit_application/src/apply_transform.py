# Inspiration from https://github.com/Klowner/inkscape-applytransforms

from inkex.transforms import Transform
from inkex.paths import Path
from bs4 import BeautifulSoup

from typing import List


IDENTITY_MATRIX = [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0]]

def _parse_matrix(str_matrix: str) -> List[List]:
    if str_matrix is None:
        return IDENTITY_MATRIX

    if str_matrix.startswith('matrix('):
        L = [float(x) for x in str_matrix[len('matrix('):-1].split(',')]
        assert len(L) == 6
        return [L[:3], L[3:]]
    else:
        print(f'Error in parse matrix: { str_matrix } ')

    return IDENTITY_MATRIX

def _recursively_apply_transform(node:BeautifulSoup, previous_transform_matrix=IDENTITY_MATRIX):

    node_transform_attr = node.get('transform')
    # matrix = _parse_matrix(node_transform_attr) if node_transform_attr else IDENTITY_MATRIX
    if node_transform_attr:
        del node.attrs['transform']
    
    total_transform = Transform(previous_transform_matrix) * Transform(node_transform_attr)
    if total_transform != Transform(IDENTITY_MATRIX):
        if node.name == 'g':
            pass
        elif node.name != 'path':
            print(f'Error. Unsupported tag { node.name }')
            return
        else:
            path_str = node.get('d')
            path = Path(path_str).to_absolute().transform(total_transform, True)
            node.attrs['d'] = str(path)

    for child in node.findChildren(recursive=False):
        _recursively_apply_transform(child, total_transform)
    

def apply_transform(svg_filename: str, output_filename: str):
    assert svg_filename.endswith('.svg')
    assert output_filename.endswith('.svg')

    f = open(svg_filename, 'r')
    soup = BeautifulSoup(f.read(), 'xml')
    f.close()

    svg = soup.find('svg')
    _recursively_apply_transform(svg)

    with open(output_filename, 'w') as f:
        f.write(str(soup))


if __name__ == '__main__':
    import sys
    if len(sys.argv) != 3:
        print('Usage: python3 apply_transform.py <my_svg_file> <output_filename>')
        sys.exit(0)
    
    filename = sys.argv[1]
    output_filename = sys.argv[2]
    apply_transform(filename, output_filename)
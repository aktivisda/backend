import streamlit as st
import os
import json
import copy
import uuid
import subprocess
import shutil

from . import svg_data
from . import streamlit_utils
from . import test_hitpath
from . import svg_checker
from . import hitpath
from . import png_utils
from . import data_utils
from . import router_utils
from . import utils
from . import hitpath_to_hull
from . import svg_utils


SYMBOLS_DATA_FILENAME = os.getenv('SYMBOLS_DATA_FILENAME')
AKTIVISDA_DIRECTORY = os.getenv('AKTIVISDA_DIRECTORY')

if not os.path.exists('tmp'):
    os.mkdir('tmp')
if not os.path.exists('tmp/previews'):
    os.mkdir('tmp/previews')

@st.cache
def save_uploaded_file_in_tmp(uploaded_file) -> str:
    new_name = uploaded_file.name
    tmp_path = os.path.join('tmp', new_name)

    if os.path.exists(tmp_path):
        os.remove(tmp_path)

    f = open(tmp_path, 'wb')
    f.write(uploaded_file.read())
    f.close()
    return tmp_path

@st.cache
def color_quantization_png_file(tmp_path: str, nb_colors: int, white_to_alpha: bool) -> str:
    assert tmp_path.endswith('png')

    data = png_utils.convert_polychromatic_png(tmp_path, nb_colors, white_to_alpha)
    st.image(
        [ tmp_path, data['quantized_image']] + data['masks'], 
        caption=['Original image', f'Quantized image with {nb_colors} colors'] + [f'color {color}' for color in data['colors']], 
        width=300)

    svg_path = tmp_path[:-4] + f'-{nb_colors}.svg'
    png_utils.convert_and_merge_to_svg(data['masks'], data['colors'], svg_path)

    f = open(svg_path, 'r')
    svg_string = f.read()
    f.close()
    svg_checker.check(svg_string)
    return svg_path


@st.cache
def load_svg(symbols_data, is_temporary_file, str_mode):
    if not is_temporary_file:
        with open(os.path.join(AKTIVISDA_DIRECTORY, 'static', f'{str_mode}s', symbols_data['filename'])) as f:
            return f.read()
    else:
        with open(os.path.join('tmp', symbols_data['filename'])) as f:
            return f.read()

def display_symbols_data(symbols_data, is_temporary_file, str_mode):
    svg_string = load_svg(symbols_data, is_temporary_file, str_mode)
    if str_mode == 'symbol':
        cols1, cols2, cols3, cols4, cols5 = st.columns(5)
    else:
        cols1, cols2, cols3 = st.columns(3)

    with cols1:
        st.write('### SVG')
        streamlit_utils.render_svg(svg_string)
        st.write(f'{symbols_data["width"]} x {symbols_data["height"]}')
        
    with cols2:
        st.write('### Colors')
        for color in symbols_data['colors']:
            st.markdown(f'<p>{ color }<span class="badge" style="background-color:{color}"></span></p>', unsafe_allow_html=True)

    with cols3:
        st.write('### Preview')
        st.image(data_utils.symbol_or_background_preview_filename(symbols_data['preview'], is_temporary=is_temporary_file, str_mode=str_mode))

        if 'restricted_preview' in symbols_data:
            st.image(data_utils.symbol_or_background_preview_filename(symbols_data['restricted_preview'], is_temporary=is_temporary_file, str_mode=str_mode))

    if str_mode == 'symbol':
        with cols4:
            st.write('### Hitpath')
            if st.button('Recompute hitpath?'):
                path = os.path.join('tmp', symbols_data['filename']) if is_temporary_file else os.path.join(AKTIVISDA_DIRECTORY, f'static/{ str_mode }s', symbols_data['filename'])
                st.session_state['hitform'] = hitpath.compute_hitpath(path, hitpath_directory=os.path.join('tmp', 'hitpaths'))[0]

            hitform = st.session_state['hitform'] if 'hitform' in st.session_state else symbols_data['hitform']
            svg_string_with_hitpath = test_hitpath.add_hitpath_to_svg_string(svg_string, hitpath=hitform, hull=None)
            streamlit_utils.render_svg(svg_string_with_hitpath)

        with cols5:
            st.write('### Hull')
            if st.button('Recompute hull?') or 'backgroundHull' not in symbols_data.keys():
                path = os.path.join('tmp', symbols_data['filename']) if is_temporary_file else os.path.join(AKTIVISDA_DIRECTORY, f'static/{ str_mode }s', symbols_data['filename'])

                hitpath_png = hitpath_to_hull.hitpath_to_png(svg_string=hitpath_to_hull.read_svg(path), hitpath=symbols_data['hitform'])

                st.session_state['backgroundHull'] = hitpath_to_hull.compute_hull(hitpath_png)

            backgroundHull = st.session_state['backgroundHull'] if 'backgroundHull' in st.session_state else symbols_data['backgroundHull']

            svg_string_with_hitpath = test_hitpath.add_hitpath_to_svg_string(svg_string, hitpath=backgroundHull, hull=None)
            streamlit_utils.render_svg(svg_string_with_hitpath)

def run(data: data_utils.Data, mode: data_utils.SymbolType):
    utils.display_style()
    symbols = data.symbols_or_backgrounds(mode)
    symbol_ids = [symbol['id'] for symbol in symbols]
    
    str_mode = data_utils.SymbolType.to_string(mode)

    st.write('''
    > This page allows you to add new symbols or update existing ones.
    ''')

    creation_mode = st.checkbox('Creation mode')
    reset = 'creation_mode' in st.session_state and st.session_state['creation_mode'] != creation_mode
    st.session_state['creation_mode'] = creation_mode


    if not creation_mode:
        with st.expander("Use gallery to select"):
            filenames = [data_utils.symbol_or_background_preview_filename(symbol['preview'], is_temporary=False, str_mode=str_mode) for symbol in symbols]
            urls = [f'/?page={ str_mode }s&{str_mode}={ symbol["id"] }' for symbol in symbols]
            st.markdown(png_utils.to_html_img_list(filenames, urls), unsafe_allow_html=True)


        symbol_id = router_utils.get_url_param_or_none('symbol')
        symbol_id_index = symbol_ids.index(symbol_id) if symbol_id is not None and symbol_id in symbol_ids else len(symbol_ids) - 1
        symbol_id = st.selectbox('Select the symbol to update', symbol_ids, symbol_id_index)
        router_utils.set_url_param('symbol', symbol_id)

        reset = reset or 'symbol_id' in st.session_state and st.session_state['symbol_id'] != symbol_id
        st.session_state['symbol_id'] = symbol_id
        
        symbol_data = symbols[symbol_ids.index(symbol_id)]
        if symbol_data['filename'].endswith('svgz'):
            st.error("'svgz' update is not yet supported. Please select another symbol.")
            return
        tags = symbol_data['tags'].split(',')
        if len(tags) == 1 and tags[0] == "":
            tags = []
        
        if 'labels' in st.session_state and not reset:
            labels = json.loads(st.session_state['labels']) 
        else:
            labels = { lang: '' for lang in data.langs }
            if 'label' in symbol_data:
                for lang, label in symbol_data['label'].items():
                    labels[lang] = label

    if creation_mode:
        uploaded_file = st.file_uploader('Please select the symbol from your computer', type=['svg', 'png', 'jpg', 'jpeg'], accept_multiple_files=False)
        if not uploaded_file:
            return
        
        symbol_id = uploaded_file.name
        reset = reset or 'symbol_id' in st.session_state and st.session_state['symbol_id'] != symbol_id
        st.session_state['symbol_id'] = symbol_id

        tmp_path = save_uploaded_file_in_tmp(uploaded_file)

        if tmp_path.endswith('jpg') or tmp_path.endswith('jpeg'):
            [basename, extension] = os.path.splitext(tmp_path)
            # TODO (create a function)
            jpg_tmp_path = tmp_path
            tmp_path = basename + '.png'
            cmd = f'convert "{jpg_tmp_path}" "{tmp_path}"'
            subprocess.call(cmd, shell=True)

        
        if tmp_path.endswith('png'):
            nb_colors = st.number_input('Nb of colors', min_value=1, max_value=10)
            white_to_alpha = mode == data_utils.SymbolType.SYMBOL
            tmp_path = color_quantization_png_file(tmp_path, nb_colors, white_to_alpha)
        
        st.write(tmp_path)
        symbol_data = svg_data.compute_symbol_data(os.path.basename(tmp_path), is_background=mode == data_utils.SymbolType.BACKGROUND)

        symbol_data['restricted_filename'] = 'color_updated' + symbol_data['filename']
        colors = symbol_data['colors']
        restricted_nb_colors = st.number_input(
            "Nb couleurs", min_value=1, max_value=len(colors), value=len(colors))
        restricted_colors = svg_utils.restrict_colors(list(colors.keys()), restricted_nb_colors)

        s = ''
        for old, new in restricted_colors.items():
            s += f'<span class="color-circle" style="background-color:{old}"></span><span class="color-circle" style="background-color:{new}"></span><span class="color-circle"></span>'
        st.write(s, unsafe_allow_html=True)


        shutil.copy(os.path.join('tmp', symbol_data['filename']), os.path.join('tmp', 'color_updated' + symbol_data['filename']))

        new_preview_path = os.path.join(
            'tmp', 'previews', 'color_updated' + os.path.splitext(symbol_data['filename'])[0] + '.png')

        svg_utils.change_color(
            'color_updated' + symbol_data['filename'], restricted_colors)

        symbol_data['restricted_colors'] = [
            "#" + c for c in svg_utils.format_and_detect_colors(os.path.join('tmp', symbol_data['restricted_filename']), symbol_data['filename'].endswith('.svgz'))]


        svg_utils.generate_preview(os.path.join('tmp', 'color_updated' + symbol_data['filename']), new_preview_path, symbol_data['width'], symbol_data['height'])
        symbol_data['restricted_preview'] = os.path.join(
            'previews', 'color_updated' + os.path.splitext(symbol_data['filename'])[0] + '.png')
        tags = []
        labels = json.loads(st.session_state['labels'])  if 'labels' in st.session_state and not reset else { lang: '' for lang in data.langs }

    with st.expander("Raw original data"):
        st.write(symbol_data) 

    if reset:
        if 'hitform' in st.session_state: 
            del st.session_state['hitform']

    st.write(f'{ str_mode } { symbol_data["id"] } created on { symbol_data["creation_date"] if "creation_date" in symbol_data else "?" }')
        
    display_symbols_data(symbol_data, is_temporary_file=creation_mode, str_mode=str_mode)

    st.write('### Tags')
    available_tags = data.tags
    tags = st.multiselect('Which tags?', available_tags, tags)

    st.write('### Labels')

    for lang, label in labels.items():
        labels[lang] = st.text_input(f'{lang}:  label', labels[lang])

    st.session_state['labels'] = json.dumps(labels)
    toolboxcol1, toolboxcol2 = st.columns(2)
    if toolboxcol1.button('Add this symbol?' if creation_mode else 'Update this symbol?'):
        new_symbol_data = copy.deepcopy(symbol_data)
        labels_edited = { lang: label for lang, label in labels.items() if label.strip() != ''} 
        new_symbol_data['label'] = labels_edited
        new_symbol_data['tags'] = ','.join(tags)

        if 'hitform' in st.session_state:
            new_symbol_data['hitform'] = st.session_state['hitform']
            
        if 'backgroundHull' in st.session_state:
            new_symbol_data['backgroundHull'] = st.session_state['backgroundHull']

        if creation_mode:
            binary = new_symbol_data['filename'].endswith('.svgz')
            extension = 'svgz' if binary else 'svg'
            new_id = str(uuid.uuid4())[:8]
            svg_checker.check(svg_utils.load_svg_string(os.path.join('tmp', new_symbol_data['filename'])))
            os.rename(os.path.join('tmp', new_symbol_data['restricted_filename']), os.path.join(AKTIVISDA_DIRECTORY, f'static/{ str_mode }s', new_id + '.' + extension))
            os.rename(os.path.join('tmp', new_symbol_data['restricted_preview']), os.path.join(AKTIVISDA_DIRECTORY, f'static/{ str_mode }s/previews', new_id + '.png'))
            new_symbol_data['id'] = new_id
            new_symbol_data['colors'] = {x: x for x in new_symbol_data['restricted_colors'] }
            new_symbol_data['filename'] = new_id + '.' + extension
            new_symbol_data['preview'] = 'previews/' + new_id + '.png'

            del new_symbol_data['restricted_colors']
            del new_symbol_data['restricted_filename']
            del new_symbol_data['restricted_preview']
            if str_mode == 'symbol':
                new_symbol_data['type'] = 'internalsvg'
            else:
                new_symbol_data['type'] = 'internalphoto'
        data.update_or_add_symbol_or_background(new_symbol_data, mode)
        data.save()

        st.success('Symbol updated/created.')
    
    if not creation_mode and toolboxcol2.button('Delete this symbol'):
        data.remove_symbol_or_background(symbol_data['id'], mode)
        data.save()
        router_utils.remove_url_param(str_mode)
        st.success('Symbol removed.')
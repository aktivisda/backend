import streamlit as st

def get_url_param_or_none(key: str):
    params = st.experimental_get_query_params()
    if key not in params.keys():
        return None
    return params[key][0]

def set_url_param(key: str, value):
    existing_params = st.experimental_get_query_params()
    if key not in existing_params.keys():
        existing_params[key] = [value]
    existing_params[key][0] = value
    st.experimental_set_query_params(**existing_params)

def remove_url_param(key: str):
    existing_params = st.experimental_get_query_params()
    if key not in existing_params.keys():
        return
    del existing_params[key]
    st.experimental_set_query_params(**existing_params)
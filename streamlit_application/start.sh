#!/bin/bash

cd app
streamlit run app.py &

cd aktivisda-data/
npm run install
npm run serve &  

wait -n
  
# Exit with status of process that exited first
exit $?


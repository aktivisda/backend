import os
import tempfile
import subprocess
from typing import Optional

from .abstract_file import AbstractFile

class SvgFile(AbstractFile):

    def __init__(self, filepath: Optional[str]=None, suffix: Optional[str]=None):
        super().__init__(filepath, suffix)
        self.check_inkscape_installation()

    @property
    def extensions(self):
        return ['.svg', '.svgz']

    @property
    def suffix(self):
        return '.svg'

    @property
    def width(self):
        command_get_width = f'inkscape "{ self.filepath }" -W'
        width = float(self.execute_command(command_get_width))
        return width

    @staticmethod
    def height(self):
        command_get_height = f'inkscape "{ self.filepath }" -H'
        height = float(self.execute_command(command_get_height))
        return height

    def check_inkscape_installation(self):
        # TODO check version and avaibilitys
        pass

    def compress_scour(self, suffix: Optional[str]=None):
        options = "--shorten-ids --indent=none --disable-simplify-colors --enable-comment-stripping --remove-descriptions --remove-metadata --remove-descriptive-elements --no-line-breaks --create-groups --set-precision 4"
        svg_file = SvgFile(suffix=suffix)
        command = f"scour '{ self.filepath }' '{ svg_file.filepath }' { options }"
        subprocess.call(command, shell=True)
        return svg_file

    def fit_to_drawing(self): # -> SvgFile
        out_file = SvgFile()
        command_fit_to_drawing = f'inkscape --batch-process --actions="file-open:{ self.filepath };select-all;fit-canvas-to-selection;export-filename:{ out_file.filepath };export-do;file-close"'
        print('fit to drawing')
        print(command_fit_to_drawing)
        self.execute_command(command_fit_to_drawing)
        return out_file

    def svg_string(self):
        if self.extension == '.svgz':
            self.compress_scour(suffix='.svg').copy(self.filepath)
        with open(self.filepath) as f:
            return f.read()

    def export_to_png(self, image_width: int, image_height: int,
                      expected_width: Optional[float] = None, expected_height: Optional[float] = None,
                      minimum_size: Optional[float] = None):

        from .pngfile import PngFile

        if expected_width is not None and expected_height is not None:
            raise ValueError(f'Bad args. Please provid expected_with or expected_height')
        if expected_width is None and expected_height is None and minimum_size is None:
            raise ValueError(f'Bad args. Please provid expected_with or expected_height')

        ratio = float(image_height) / image_width

        if expected_width is not None:
            new_width = expected_width
            new_height = new_width * ratio
        elif expected_height is not None:
            new_height = expected_height
            new_width = new_height / ratio
        elif image_width <= image_height:
            new_width = minimum_size
            new_height = new_width * ratio
        else:
            new_height = minimum_size
            new_width = new_height / ratio

        new_height = int(round(new_height))
        new_width = int(round(new_width))

        out_file = PngFile()
        command = f'inkscape -w { new_width } "{ self.filepath }" --export-background=white -o "{ out_file.filepath }"'
        self.execute_command(command)
        return out_file


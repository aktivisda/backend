from typing import Optional, List, Tuple

from .abstract_file import AbstractFile

import cv2
import copy
import numpy as np
from sklearn.cluster import KMeans
from sklearn.utils import shuffle
from matplotlib import colors


class PngFile(AbstractFile):

    def __init__(self, filepath: Optional[str]=None, suffix: Optional[str]=None):
        super().__init__(filepath, suffix)

    @property
    def extensions(self):
        return ['.png']

    @property
    def suffix(self):
        return '.png'

    @property
    def width(self):
        command = f'identify -format "%w" "{ self.filepath }"'
        return float(self.execute_command(command))

    @property
    def height(self):
        command = f'identify -format "%h" "{ self.filepath }"'
        return float(self.execute_command(command))

    def compress(self, output_filepath: Optional[str]=None): # -> PngFile:
        result = PngFile(output_filepath)
        command = f'pngquant "{ self.filepath }" --output "{ result.filepath }"'
        self.execute_command(command)
        return result


    def posterize(se
import os
import subprocess
from typing import Optional

class InkscapeCommands:

    @staticmethod
    def check_inkscape_installation():
        # TODO check version and avaibilitys
        pass


    @staticmethod
    def check_svg_filename(filename):
        ''''''
        if not os.path.exists(filename):
            raise FileNotFoundError(f'File "{ filename }" does not exist')
        if not filename.endswith('.svg') and not filename.endswith('.svgz'):
            raise NotImplementedError(
                f'Inkscape commands requires svg or svgz files (not "{ filename }").')

    @staticmethod
    def execute_command(command: str, verbose=True):
        # TODO handle errors
        print(command)
        InkscapeCommands.check_inkscape_installation()
        try:
            str_result = subprocess.run(command, shell=True,
                        stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.decode('utf8')
        except Exception as e:
            print('---------------------------------------------------------------------')
            print(e)
        print('===============================================================')
        print(str_result)
        return str_result

    @staticmethod
    def fit_to_drawing(filename: str):
        # TODO decorator ?
        InkscapeCommands.check_svg_filename(filename)
        # TODO possible bug if filename contains whitespace?
        command_fit_to_drawing = f'inkscape --batch-process --actions="file-open:{ filename };select-all;fit-canvas-to-selection;export-filename:{ filename };export-do;file-close"'
        InkscapeCommands.execute_command(command_fit_to_drawing)

    @staticmethod
    def get_image_width(filename: str):
        InkscapeCommands.check_svg_filename(filename)
        command_get_width = f'inkscape "{ filename }" -W'
        width = float(InkscapeCommands.execute_command(command_get_width))
        return width

    @staticmethod
    def get_image_height(filename: str):
        InkscapeCommands.check_svg_filename(filename)
        command_get_height = f'inkscape "{ filename }" -H'
        height = float(InkscapeCommands.execute_command(command_get_height))
        return height

    @staticmethod
    def export_to_png(image_width: int, image_height: int, in_filename: str, out_filename: str,
                      expected_width: Optional[float] = None, expected_height: Optional[float] = None,
                      minimum_size: Optional[float] = None):
        if expected_width is not None and expected_height is not None:
            raise ValueError(f'Bad args. Please provid expected_with or expected_height')
        if expected_width is None and expected_height is None and minimum_size is None:
            raise ValueError(f'Bad args. Please provid expected_with or expected_height')

        InkscapeCommands.check_svg_filename(in_filename)

        ratio = float(image_height) / image_width

        # TODO check out_filename: does not exist and endswith png
        if expected_width is not None:
            new_width = expected_width
            new_height = new_width * ratio
        elif expected_height is not None:
            new_height = expected_height
            new_width = new_height / ratio
        elif image_width <= image_height:
            new_width = minimum_size
            new_height = new_width * ratio
        else:
            new_height = minimum_size
            new_width = new_height / ratio

        new_height = int(round(new_height))
        new_width = int(round(new_width))

        command = f'inkscape -w { new_width } "{ in_filename }" --export-background=white -o "{ out_filename }"'
        InkscapeCommands.execute_command(command)
        return (new_width, new_height)
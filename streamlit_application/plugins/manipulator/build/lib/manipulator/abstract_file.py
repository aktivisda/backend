from typing import Optional, List
import subprocess
import tempfile
import shutil
import os


class AbstractFile:

    _filepath: str
    _is_temporary_file: bool

    def __init__(self, filepath: Optional[str]=None, suffix: Optional[str]=None):

        if filepath is not None:
            self._filepath = filepath
            self.check_filepath()
            self._is_temporary_file = False
        else:
            self._filepath = tempfile.NamedTemporaryFile(
                delete=False, suffix=suffix if suffix is not None else self.suffix).name
            self._is_temporary_file = True


    def __del__(self):
        if self._is_temporary_file:
            os.unlink(self.filepath)

    def copy(self, output_path, force=False):
        if os.path.exists(output_path) and not force:
            raise ValueError(f' { output_path } already exists')
        shutil.copy(self.filepath, output_path)

    @property
    def filepath(self) -> str:
        return self._filepath

    @property
    def extension(self) -> str:
        return os.path.splitext(self._filepath)[1]

    @property
    def suffix(self) -> str:
        raise NotImplementedError('Suffix should be overriden')

    @property
    def extensions(self) -> List[str]:
        raise NotImplementedError('extensions should be overriden')

    def check_filepath(self):
        '''Check if fillename points to a svg/svgz filename'''
        if not os.path.exists(self.filepath):
            raise FileNotFoundError(f'File "{ self.filepath }" does not exist')
        for extension in self.extensions:
            if self.filepath.endswith(extension):
                break
        else:
            raise NotImplementedError(
                f'Inkscape commands requires extensions in { self.extensions } files (not "{ self.filepath }").')

    def execute_command(self, command: str, verbose=True):
        '''Execute the inkscape command'''
        # TODO handle errors
        if verbose:
            print(command)
        str_result = subprocess.run(command, shell=True,
                                    stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.decode('utf8')
        return str_result

from flask import Flask, request, send_file
from images_manipulator import PngFile

import tempfile

app = Flask(__name__)


@app.route("/vectorize", methods=['POST'])
def vectorize():
    if request.method == 'POST':

        f = request.files['image']
        assert f.filename.endswith('.png')

        tempfilename = tempfile.NamedTemporaryFile(delete=True, suffix='.png').name
        f.save(tempfilename)

        png_file = PngFile(tempfilename)
        nb_colors = int(request.args.get('nb_colors', 1))
        svg_file = png_file.to_svg(nb_colors=nb_colors)
        svg_file.format_colors()
        return send_file(svg_file.filepath, mimetype='image/svg')



import argparse
import dotenv
import sys


dotenv.load_dotenv('.env')
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Manage')
    subparsers = parser.add_subparsers(help='sub-command help')

    parser_users_create = subparsers.add_parser('convert', help='Convert and create all the data for a specific image')
    parser_users_create.add_argument('--image', type=str, required=True)
    # parser_users_create.set_defaults(func=xxx)


    if len(sys.argv) == 1:
        parser.print_usage()
        sys.exit(1)
    args = parser.parse_args()
    args.func(args)

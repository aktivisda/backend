# Aktivisda - Backoffice

Backtista contains all the computational logic (vectorization), it depends on [Files Manipulator](https://framagit.org/Marc-AntoineA/files-manipulator) to manipulate files. Functions can be directly through cli or through streamlit app or on a server.

## Streamlit Application

For local use. Docker container available.

